package utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProjectProperties {
    private static final String PROJECT_PROPERTIES_FILE = "src/main/resources/properties/env/dev.properties";
    private static final Properties properties = new Properties();

    public static void load() {
        loadPropertiesFromFiles(Collections.singletonList(PROJECT_PROPERTIES_FILE));
        properties.forEach((key, value) -> System.setProperty((String) key, (String) value));
    }

    private static void loadPropertiesFromFiles(final List<String> propertyFiles) {
        propertyFiles.forEach(file -> {
            try (InputStream input = Files.newInputStream(Paths.get(file))) {
                properties.load(input);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public static String getBaseUrl() {
        return Optional.ofNullable(System.getProperty("baseUrl")).orElse("https://swapi.dev/api");
    }
}