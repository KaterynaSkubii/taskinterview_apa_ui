package services;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import io.restassured.response.Response;
import lombok.Data;
import lombok.EqualsAndHashCode;
import services.data.DataForTesting;
import services.models.Model;

import static io.restassured.RestAssured.given;

@Data
@EqualsAndHashCode(callSuper = true)
public class RestClient extends DataForTesting {

    private RequestSpecification requestSpecification;
    private Response response;

    public RestClient(String baseUri) {
        requestSpecification = new RequestSpecBuilder().setBaseUri(baseUri).build();
    }


    // *************************************** Operations *************************************************

    public Response sendGetRequest(String url) {
        return response = given().spec(requestSpecification).get(url);
    }

    // **************************************** Extracting data **********************************************

    public Model extractResponseData(String url, Class<? extends Model> className) {
        return sendGetRequest(url.replaceAll(REGEX_FOR_URL, ""))
                .then().statusCode(200).extract().as(className);
    }
}