package services.data;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class DataForTesting {

    protected static final String URL_PATH_FOR_FILMS = "/films";
    protected static final String URL_PATH_FOR_PEOPLES = "/people";
    protected static final String NAME_OF_FILM = "A New Hope";
    protected static final String NAME_OF_CHARACTER_LUKE_SKYWALKER = "Luke Skywalker";
    protected static final String NAME_OF_CHARACTER_BIGGS_DARKLIGHTER = "Biggs Darklighter";
    protected static final String EXPECTED_STARSHIP_CLASS = "Starfighter";

    protected static final String REGEX_FOR_URL = "[\\[\\]]";

    protected static final String MESSAGE_FOR_EXCEPTION = "\"Url for Starshop is not found\"";
}