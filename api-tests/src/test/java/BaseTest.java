import org.testng.annotations.BeforeTest;
import org.testng.asserts.SoftAssert;
import services.RestClient;
import services.data.DataForTesting;
import services.models.*;
import utils.ProjectProperties;

import java.util.stream.Collectors;

import static utils.ProjectProperties.getBaseUrl;

public abstract class BaseTest extends DataForTesting {

    static {
        ProjectProperties.load();
    }

    protected String urlLukeSkywalker;

    protected SoftAssert softAssert = new SoftAssert();
    protected RestClient restClient = new RestClient(getBaseUrl());
    protected ListOfFilmsResponse listOfFilmsResponse = new ListOfFilmsResponse();
    protected ListOfPeoplesResponse listOfPeoplesResponse = new ListOfPeoplesResponse();
    protected StarshipsResponse starshipsResponse = new StarshipsResponse();
    protected FilmResponse filmResponse = new FilmResponse();
    protected PeopleResponse peopleResponse = new PeopleResponse();

    @BeforeTest()
    public void preconditions() {
        listOfPeoplesResponse = (ListOfPeoplesResponse) restClient.extractResponseData(
                URL_PATH_FOR_PEOPLES, ListOfPeoplesResponse.class);

        urlLukeSkywalker = listOfPeoplesResponse.getListOfPeoples().stream()
                .filter(p -> p.getName().equals(NAME_OF_CHARACTER_LUKE_SKYWALKER))
                .map(PeopleResponse::getUrl)
                .collect(Collectors.joining());
    }
}