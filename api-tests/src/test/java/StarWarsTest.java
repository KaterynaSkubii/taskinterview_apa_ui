import org.testng.annotations.Test;
import services.models.*;

import java.util.stream.Collectors;

import static org.testng.AssertJUnit.assertTrue;

public class StarWarsTest extends BaseTest {

    private String urlBiggsDarklighter;
    private String urlForFilmANewHope;

    @Test()
    public void testingTaskTwo() throws Exception {
        findFilmWithATitleANewHope().findPersonWithNameBiggsDarklighter().findWhichStarshipBiggsDarklighterWasFlyingOn();
        softAssert.assertEquals(starshipsResponse.getStarshipClass(), EXPECTED_STARSHIP_CLASS);
        softAssert.assertTrue(starshipsResponse.getPilots().stream().anyMatch(p -> p.equals(urlLukeSkywalker)));
        softAssert.assertAll();
    }

    // ****************************************** Additional methods ***********************************************

    private StarWarsTest findFilmWithATitleANewHope() {
        listOfFilmsResponse = (ListOfFilmsResponse) restClient.extractResponseData(
                URL_PATH_FOR_FILMS, ListOfFilmsResponse.class);
        urlForFilmANewHope = listOfFilmsResponse.getListOfFilms().stream()
                .filter(f -> f.getTitle().equals(NAME_OF_FILM))
                .map(FilmResponse::getUrl)
                .collect(Collectors.joining());
        return this;
    }

    private StarWarsTest findPersonWithNameBiggsDarklighter() {
        filmResponse = (FilmResponse) restClient.extractResponseData(urlForFilmANewHope, FilmResponse.class);
        urlBiggsDarklighter = listOfPeoplesResponse.getListOfPeoples().stream()
                .filter(p -> p.getName().equals(NAME_OF_CHARACTER_BIGGS_DARKLIGHTER))
                .map(PeopleResponse::getUrl)
                .collect(Collectors.joining());
        assertTrue(filmResponse.getCharacters().stream().anyMatch(n -> n.equals(urlBiggsDarklighter)));
        return this;
    }

    private void findWhichStarshipBiggsDarklighterWasFlyingOn() throws Exception {
        peopleResponse = (PeopleResponse) restClient.extractResponseData(urlBiggsDarklighter, PeopleResponse.class);
        String urlForStarship = peopleResponse.getStarships().stream().findFirst()
                .orElseThrow(() -> new Exception(MESSAGE_FOR_EXCEPTION));
        starshipsResponse = (StarshipsResponse) restClient.extractResponseData(urlForStarship, StarshipsResponse.class);
    }
}