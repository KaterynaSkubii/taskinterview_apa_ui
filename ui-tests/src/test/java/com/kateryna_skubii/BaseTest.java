package com.kateryna_skubii;

import com.kateryna_skubii.utils.ProjectProperties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import static com.kateryna_skubii.utils.ProjectProperties.*;

public abstract class BaseTest {

    static {
        ProjectProperties.load();
    }

    protected WebDriver driver;

    @BeforeClass
    protected void before() {
        System.setProperty(getWebdriver(), getPathToWebdriver());
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(getTimeout(), TimeUnit.SECONDS);
    }

    @AfterClass
    protected void after() {
        if (driver != null) {
            driver.quit();
        }
    }
}