package com.kateryna_skubii;

import com.kateryna_skubii.pages.XMHomePage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.kateryna_skubii.utils.ProjectProperties.getBaseUrl;

public class FirstTaskTest extends BaseTest {

    protected static final String FRAME_ID = "iFrameResizer0";

    private static final String EXPECTED_URL_OF_ECONOMIC_CALENDAR_PAGE = "https://www.xm.com/research/economicCalendar";
    private static final String EXPECTED_URL_OF_DISCLAIMER_PAGE = "https://www.xm.com/research/risk_warning";
    private static final String EXPECTED_URL_OF_RISK_WARNING_PAGE =
            "https://cloud.xm-cdn.com/static/pdf/System-PDFs/XM-Risk-Disclosures-for-Financial-Instruments.pdf?v=" +
                    "fa753a4995a2e1315d80850da95c2537";

    private XMHomePage xmHomePage;
    @BeforeMethod
    public void initialize() {
        xmHomePage = new XMHomePage(driver);
    }

    @Test
    public void firstTaskTest() {
        xmHomePage
                .openPage(getBaseUrl())
                .acceptCookies()
                .clickResearchAndEducationButton()
                .clickEconomicCalendarButton()
                .isOnPage(EXPECTED_URL_OF_ECONOMIC_CALENDAR_PAGE)
                .toIFrame(FRAME_ID)
 // *** The implementation of the methods of the steps below still needs to be improved, since on the Economic Calendar
 // page the elements for the specified locators are not found and are not clickable. When trying to do this in the
 // standard way, the code throws an error org.openqa.selenium.NoSuchElementException, however, the element can be
 // found by the specified locator (xpath) through the developer panel in the browser. Perhaps this is due to how the
 // Frame arranges, but in the previous step it was done checking if the given frame is available for switching and it
 // has been passed
                .clickHereLinkInTheDisclaimerBlock()
                .isOnPage(EXPECTED_URL_OF_DISCLAIMER_PAGE)
                .clickHereLinkInTheRiskWarningBlock()
                .isOnPage(EXPECTED_URL_OF_RISK_WARNING_PAGE)
                .clickTodayButtonFromCalendar();
    }
}