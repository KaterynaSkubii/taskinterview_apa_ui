package com.kateryna_skubii.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProjectProperties {
    private static final String ENV_PROJECT_PROPERTIES_FILE = "src/main/resources/properties/env/dev.properties";
    private static final String CONFIG_PROJECT_PROPERTIES_FILE = "src/main/resources/properties/configs/config.properties";
    private static final Properties properties = new Properties();

    public static void load() {
        loadPropertiesFromFiles(Arrays.asList(ENV_PROJECT_PROPERTIES_FILE, CONFIG_PROJECT_PROPERTIES_FILE));
        properties.forEach((key, value) -> System.setProperty((String) key, (String) value));
    }

    private static void loadPropertiesFromFiles(final List<String> propertyFiles) {
        propertyFiles.forEach(file -> {
            try (InputStream input = Files.newInputStream(Paths.get(file))) {
                properties.load(input);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public static String getBaseUrl() {
        return Optional.ofNullable(System.getProperty("baseUrl")).orElse("baseUrl=https://www.xm.com");
    }

    public static String getWebdriver() {
        return Optional.ofNullable(System.getProperty("webdriver")).orElse("webdriver.chrome.driver");
    }

    public static String getPathToWebdriver() {
        return Optional.ofNullable(System.getProperty("pathToWebdriver")).orElse("src/main/resources/driver/chromedriver.exe");
    }

    public static int getTimeout() {
        return Optional.of(Integer.parseInt(System.getProperty("timeout"))).orElse(45);
    }
}