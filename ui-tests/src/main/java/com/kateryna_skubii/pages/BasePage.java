package com.kateryna_skubii.pages;

import com.kateryna_skubii.utils.ProjectProperties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.kateryna_skubii.utils.ProjectProperties.getTimeout;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

public abstract class BasePage {

    static {
        ProjectProperties.load();
    }

    protected final WebDriver driver;
    protected final Actions actions;
    protected final Wait<WebDriver> webDriverWait;

    BasePage(WebDriver driver) {
        this.driver = driver;
        webDriverWait = new WebDriverWait(driver, getTimeout()).withMessage("Element was not found");
        actions = new Actions(driver);
        PageFactory.initElements(driver,this);
    }

    // checking
    public EconomicCalendarPage isOnPage(String url) {
        assertEquals(driver.getCurrentUrl(), url);
        return new EconomicCalendarPage(driver);
    }

    // action
    protected void clickWithActionAndWaiters(WebElement block, WebElement buttonInTheBlock) {
        isElementLoaded(block);
        waitForToBeClickable(buttonInTheBlock);
        actions.moveToElement(buttonInTheBlock).click().build().perform();
    }

    // wait
    private void waitForToBeClickable(WebElement webElement) {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(webElement));
    }

    private void waitForVisibilityOf(WebElement webElement) {
        webDriverWait.until(ExpectedConditions.visibilityOf(webElement));
    }

    private void isElementLoaded(WebElement webElement) {
        waitForVisibilityOf(webElement);
        assertTrue(webElement.isDisplayed());
    }
}