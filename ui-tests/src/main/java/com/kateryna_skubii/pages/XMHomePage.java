package com.kateryna_skubii.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class XMHomePage extends BasePage {

    @FindBy(xpath = "//li/parent::ul[@id='main-nav']")
    private WebElement horizontalNavigation;
    @FindBy(xpath = "//li[@class='main_nav_research']")
    private WebElement researchAndEducationButton;
    @FindBy(xpath = "//div[@class='dropdown']//parent::li/a[contains(text(),'Economic Calendar')]")
    private WebElement dropdownMenuBlock;
    @FindBy(xpath = "//li/a[contains(text(),'Economic Calendar')]")
    private WebElement economicCalendarButton;
    @FindBy(xpath = "//div[@class='modal-content']")
    private WebElement acceptCookiesPopUp;
    @FindBy(xpath = "//div[@class='modal-content']//button[contains(text(),'ACCEPT ALL')]")
    private WebElement acceptAllCookiesButton;

    public XMHomePage(WebDriver driver) {
        super(driver);
    }

    public XMHomePage openPage(String url) {
        driver.get(url);
        return this;
    }

    public XMHomePage acceptCookies() {
        clickWithActionAndWaiters(acceptCookiesPopUp, acceptAllCookiesButton);
        return this;
    }
    public XMHomePage clickResearchAndEducationButton() {
        clickWithActionAndWaiters(horizontalNavigation, researchAndEducationButton);
        return this;
    }

    public XMHomePage clickEconomicCalendarButton() {
        clickWithActionAndWaiters(dropdownMenuBlock, economicCalendarButton);
        return this;
    }
}