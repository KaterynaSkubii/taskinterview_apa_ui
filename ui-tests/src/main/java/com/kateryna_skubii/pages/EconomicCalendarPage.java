package com.kateryna_skubii.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class EconomicCalendarPage extends BasePage {

    @FindBy(xpath = "//div//*[@class='mat-calendar']")
    private WebElement calendarBlock;
    @FindBy(xpath = "//button[@aria-current='date']")
    private WebElement todayButtonFromCalendar;
    @FindBy(xpath = "//div[@class='container mt-400']/div")
    private WebElement disclaimerBlock;
    @FindBy(xpath = "//div[@class='container mt-400']//a[@href]")
    private WebElement hereLinkInTheDisclaimerBlock;
    @FindBy(xpath = "//div[@id='risk-block']")
    private WebElement riskWarningBlock;
    @FindBy(xpath = "//div[@id='risk-block']//a[@href]")
    private WebElement hereLinkInTheRiskWarningBlock;

    public EconomicCalendarPage(WebDriver driver) {
        super(driver);
    }

    public EconomicCalendarPage clickHereLinkInTheDisclaimerBlock() {
        clickWithActionAndWaiters(disclaimerBlock, hereLinkInTheDisclaimerBlock);
        return this;
    }

    public EconomicCalendarPage clickHereLinkInTheRiskWarningBlock() {
        clickWithActionAndWaiters(riskWarningBlock, hereLinkInTheRiskWarningBlock);
        return this;
    }

    public void clickTodayButtonFromCalendar() {
        clickWithActionAndWaiters(calendarBlock, todayButtonFromCalendar);
    }

    // Iframe
    public EconomicCalendarPage toIFrame(String frameId) {
        webDriverWait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameId));
        return this;
    }
}